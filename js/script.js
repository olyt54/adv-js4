'use strict'

// AJAX это подход, который позволяет общаться с сервером, посылая на него запросы
// нужен для того, что так как js не поддерживает многопоточность, позволяет её имитировать
// выполняя код асинхронно

function showFilms() {
    fetch("https://swapi.dev/api/films")
        .then(res => res.json())
        .then(data => {
            data.results.forEach(item => {
                document.body.prepend(createEpisodeItem(item));
            })
        })
}

function createEpisodeItem (sourceObject) {
    const episodeWrapper = document.createElement("div"),
        id = document.createElement("span"),
        titleHead = document.createElement("h3"),
        opening = document.createElement("p");

    episodeWrapper.classList.add("wrapper");
    id.classList.add("id");
    titleHead.classList.add("title");
    opening.classList.add("opening");

    const {title, episode_id, opening_crawl, characters} = sourceObject;

    id.textContent = episode_id;
    titleHead.textContent = title;
    opening.textContent = opening_crawl;

    const preLoader = createAnimation();

    episodeWrapper.append(id, titleHead, preLoader, getCharactersList(characters, preLoader), opening);

    return episodeWrapper;
}

function getCharactersList (arr, preLoader) {
    const charactersList = document.createElement("ul");

    charactersList.classList.add("char-list");

    Promise.all(arr.map(item => fetch(item)))
    .then(results => {
        return Promise.all(results.map(item => item.json()))
    }).then(data => {
        data.forEach(item => {
            const charItem = document.createElement("li"),
                {name} = item;

            charItem.textContent = name;
            charactersList.append(charItem);
        });
        preLoader.classList.add("hide");
    })

    return charactersList;
}

function createAnimation() {
    const wrapper = document.createElement("div"),
        firstEl = document.createElement("div"),
        secondEl = document.createElement("div");

    wrapper.classList.add("spinner");
    firstEl.classList.add("bubble-1");
    secondEl.classList.add("bubble-2");

    wrapper.append(firstEl, secondEl);

    return wrapper;
}

showFilms();


